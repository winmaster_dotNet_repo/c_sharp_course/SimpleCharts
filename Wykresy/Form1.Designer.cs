﻿namespace Wykresy
{
    partial class FormWykresy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.zedGraphControlWykres = new ZedGraph.ZedGraphControl();
            this.textBoxTitle = new System.Windows.Forms.TextBox();
            this.textBoxTtitleX = new System.Windows.Forms.TextBox();
            this.textBoxTtitleY = new System.Windows.Forms.TextBox();
            this.textBoxX = new System.Windows.Forms.TextBox();
            this.textBoxY = new System.Windows.Forms.TextBox();
            this.labelTitle = new System.Windows.Forms.Label();
            this.labelTitleX = new System.Windows.Forms.Label();
            this.labelTitleY = new System.Windows.Forms.Label();
            this.labelX = new System.Windows.Forms.Label();
            this.labelY = new System.Windows.Forms.Label();
            this.buttonGenerate = new System.Windows.Forms.Button();
            this.labelHelp = new System.Windows.Forms.Label();
            this.buttonClear = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // zedGraphControlWykres
            // 
            this.zedGraphControlWykres.Location = new System.Drawing.Point(12, 28);
            this.zedGraphControlWykres.Name = "zedGraphControlWykres";
            this.zedGraphControlWykres.Size = new System.Drawing.Size(598, 398);
            this.zedGraphControlWykres.TabIndex = 0;
            this.zedGraphControlWykres.Load += new System.EventHandler(this.zedGraphControlWykres_Load);
            // 
            // textBoxTitle
            // 
            this.textBoxTitle.Location = new System.Drawing.Point(667, 46);
            this.textBoxTitle.Name = "textBoxTitle";
            this.textBoxTitle.Size = new System.Drawing.Size(216, 20);
            this.textBoxTitle.TabIndex = 1;
            // 
            // textBoxTtitleX
            // 
            this.textBoxTtitleX.Location = new System.Drawing.Point(667, 104);
            this.textBoxTtitleX.Name = "textBoxTtitleX";
            this.textBoxTtitleX.Size = new System.Drawing.Size(216, 20);
            this.textBoxTtitleX.TabIndex = 2;
            // 
            // textBoxTtitleY
            // 
            this.textBoxTtitleY.Location = new System.Drawing.Point(667, 151);
            this.textBoxTtitleY.Name = "textBoxTtitleY";
            this.textBoxTtitleY.Size = new System.Drawing.Size(216, 20);
            this.textBoxTtitleY.TabIndex = 3;
            // 
            // textBoxX
            // 
            this.textBoxX.Location = new System.Drawing.Point(667, 207);
            this.textBoxX.Multiline = true;
            this.textBoxX.Name = "textBoxX";
            this.textBoxX.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxX.Size = new System.Drawing.Size(100, 193);
            this.textBoxX.TabIndex = 4;
            // 
            // textBoxY
            // 
            this.textBoxY.Location = new System.Drawing.Point(783, 207);
            this.textBoxY.Multiline = true;
            this.textBoxY.Name = "textBoxY";
            this.textBoxY.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxY.Size = new System.Drawing.Size(100, 193);
            this.textBoxY.TabIndex = 5;
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.Location = new System.Drawing.Point(664, 28);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(103, 13);
            this.labelTitle.TabIndex = 6;
            this.labelTitle.Text = "Podaj tytuł wykresu:";
            // 
            // labelTitleX
            // 
            this.labelTitleX.AutoSize = true;
            this.labelTitleX.Location = new System.Drawing.Point(664, 88);
            this.labelTitleX.Name = "labelTitleX";
            this.labelTitleX.Size = new System.Drawing.Size(85, 13);
            this.labelTitleX.TabIndex = 7;
            this.labelTitleX.Text = "Podaj opis osi X:";
            // 
            // labelTitleY
            // 
            this.labelTitleY.AutoSize = true;
            this.labelTitleY.Location = new System.Drawing.Point(664, 135);
            this.labelTitleY.Name = "labelTitleY";
            this.labelTitleY.Size = new System.Drawing.Size(85, 13);
            this.labelTitleY.TabIndex = 8;
            this.labelTitleY.Text = "Podaj opis osi Y:";
            // 
            // labelX
            // 
            this.labelX.AutoSize = true;
            this.labelX.Location = new System.Drawing.Point(664, 191);
            this.labelX.Name = "labelX";
            this.labelX.Size = new System.Drawing.Size(105, 13);
            this.labelX.TabIndex = 9;
            this.labelX.Text = "Podaj argumenty (X):";
            // 
            // labelY
            // 
            this.labelY.AutoSize = true;
            this.labelY.Location = new System.Drawing.Point(780, 191);
            this.labelY.Name = "labelY";
            this.labelY.Size = new System.Drawing.Size(95, 13);
            this.labelY.TabIndex = 10;
            this.labelY.Text = "Podaj wartości (Y):";
            // 
            // buttonGenerate
            // 
            this.buttonGenerate.Location = new System.Drawing.Point(667, 417);
            this.buttonGenerate.Name = "buttonGenerate";
            this.buttonGenerate.Size = new System.Drawing.Size(102, 23);
            this.buttonGenerate.TabIndex = 11;
            this.buttonGenerate.Text = "Generuj wykres";
            this.buttonGenerate.UseVisualStyleBackColor = true;
            this.buttonGenerate.Click += new System.EventHandler(this.buttonGenerate_Click);
            // 
            // labelHelp
            // 
            this.labelHelp.AutoSize = true;
            this.labelHelp.Location = new System.Drawing.Point(9, 440);
            this.labelHelp.Name = "labelHelp";
            this.labelHelp.Size = new System.Drawing.Size(242, 13);
            this.labelHelp.TabIndex = 12;
            this.labelHelp.Text = "*Uwaga: argumenty i wartości podawać wierszami";
            // 
            // buttonClear
            // 
            this.buttonClear.Location = new System.Drawing.Point(773, 417);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(102, 23);
            this.buttonClear.TabIndex = 13;
            this.buttonClear.Text = "Wyczyść";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // FormWykresy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(914, 472);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.labelHelp);
            this.Controls.Add(this.buttonGenerate);
            this.Controls.Add(this.labelY);
            this.Controls.Add(this.labelX);
            this.Controls.Add(this.labelTitleY);
            this.Controls.Add(this.labelTitleX);
            this.Controls.Add(this.labelTitle);
            this.Controls.Add(this.textBoxY);
            this.Controls.Add(this.textBoxX);
            this.Controls.Add(this.textBoxTtitleY);
            this.Controls.Add(this.textBoxTtitleX);
            this.Controls.Add(this.textBoxTitle);
            this.Controls.Add(this.zedGraphControlWykres);
            this.Name = "FormWykresy";
            this.Text = "Wykresy";
            this.Load += new System.EventHandler(this.FormWykresy_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ZedGraph.ZedGraphControl zedGraphControlWykres;
        private System.Windows.Forms.TextBox textBoxTitle;
        private System.Windows.Forms.TextBox textBoxTtitleX;
        private System.Windows.Forms.TextBox textBoxTtitleY;
        private System.Windows.Forms.TextBox textBoxX;
        private System.Windows.Forms.TextBox textBoxY;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Label labelTitleX;
        private System.Windows.Forms.Label labelTitleY;
        private System.Windows.Forms.Label labelX;
        private System.Windows.Forms.Label labelY;
        private System.Windows.Forms.Button buttonGenerate;
        private System.Windows.Forms.Label labelHelp;
        private System.Windows.Forms.Button buttonClear;
    }
}

