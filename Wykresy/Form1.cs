﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;

/*
Author ZedGraph
JChampion http://www.codeproject.com/csharp/ZedGraph.asp
*/

namespace Wykresy
{


    public partial class FormWykresy : Form
    {

        String title = "Tytuł wykresu";
        String titleX = "Tytuł dla zmiennych X";
        String titleY = "Tytuł dla zmiennych Y";
        int sizeX;
        int sizeY;
        double[] x;
        double[] y;
        double[] x0= { };
        double[] y0= { };
        CurveItem linia;

        public FormWykresy()
        {
            InitializeComponent();
        }

        
        private void FormWykresy_Load(object sender, EventArgs e)
        {

        }

        private void zedGraphControlWykres_Load(object sender, EventArgs e)
        {
          
            zedGraphControlWykres.GraphPane.Title = title;
            
            zedGraphControlWykres.GraphPane.XAxis.Title = titleX;
            zedGraphControlWykres.GraphPane.YAxis.Title = titleY;
          
            zedGraphControlWykres.GraphPane.AxisChange();

          
            zedGraphControlWykres.GraphPane.XAxis.IsShowGrid = true;
            zedGraphControlWykres.GraphPane.YAxis.IsShowGrid = true;
            zedGraphControlWykres.GraphPane.XAxis.GridColor = Color.Gray;
            zedGraphControlWykres.GraphPane.YAxis.GridColor = Color.Gray;
        }

        public Color colorPicker()
        {
            Color[] colors = {Color.Gray, Color.Blue, Color.Red, Color.Yellow, Color.Pink, Color.Red, Color.Orange, Color.Green, Color.Black };

            Random gen = new Random();

            int m = gen.Next(0,colors.Length);

            return colors[m];
        }
      
        private void buttonGenerate_Click(object sender, EventArgs e)
        {
            
            zedGraphControlWykres.GraphPane.Title = textBoxTitle.Text;
            zedGraphControlWykres.GraphPane.XAxis.Title = textBoxTtitleX.Text;
            zedGraphControlWykres.GraphPane.YAxis.Title = textBoxTtitleY.Text;

            String []  xString = textBoxX.Text.Split('\n');
            String []  yString = textBoxY.Text.Split('\n');

            sizeX = xString.Length;
            sizeY = yString.Length;
            x = new double[sizeX];
            y = new double[sizeY];


            if(textBoxX.Text=="" || textBoxY.Text == "")
            {
                MessageBox.Show("Błędne parametry lub parametry puste");
                Close();
            }


            try
            {
                for (int i = 0; i < sizeX; i++)
                {
                    x[i] = Double.Parse(xString[i]);
                }

                for (int i = 0; i < sizeY; i++)
                {
                    y[i] = Double.Parse(yString[i]);
                }
            }
            catch(Exception)
            {
                MessageBox.Show("Błędne parametry lub parametry puste");
                Close();
            }
            
            linia= zedGraphControlWykres.GraphPane.AddCurve("Linia wykresu", x, y, colorPicker(), SymbolType.Diamond);
           
            zedGraphControlWykres.GraphPane.AxisChange();

            //siatka na wykresie
            zedGraphControlWykres.GraphPane.XAxis.IsShowGrid = true;
            zedGraphControlWykres.GraphPane.YAxis.IsShowGrid = true;
            zedGraphControlWykres.GraphPane.XAxis.GridColor = Color.Gray;
            zedGraphControlWykres.GraphPane.YAxis.GridColor = Color.Gray;

            zedGraphControlWykres.Refresh();


        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            zedGraphControlWykres.GraphPane.Title = title;
            zedGraphControlWykres.GraphPane.XAxis.Title = titleX;
            zedGraphControlWykres.GraphPane.YAxis.Title = titleY;
            
            zedGraphControlWykres.GraphPane.AxisChange();
            zedGraphControlWykres.GraphPane.XAxis.IsShowGrid = true;
            zedGraphControlWykres.GraphPane.YAxis.IsShowGrid = true;
            zedGraphControlWykres.GraphPane.XAxis.GridColor = Color.Gray;
            zedGraphControlWykres.GraphPane.YAxis.GridColor = Color.Gray;

            zedGraphControlWykres.GraphPane.CurveList.Clear();
            zedGraphControlWykres.Refresh();

            

            textBoxTitle.Text= "";
            textBoxTtitleX.Text = "";
            textBoxTtitleY.Text = "";
            textBoxX.Text = "";
            textBoxY.Text = "";
        }
    }
}
